import { NgModule } from "@angular/core";
import { PreloadAllModules, RouterModule, Routes } from "@angular/router";

const routes: Routes = [
  {
    path: "",
    redirectTo: "login",
    pathMatch: "full"
  },
  {
    path: "home",
    loadChildren: () => import("./tabs/tabs.module").then(m => m.TabsPageModule)
  },

  { path: "login", loadChildren: "./login/login.module#LoginPageModule" },
  {
    path: "register",
    loadChildren: "./register/register.module#RegisterPageModule"
  },
  {
    path: "registersensor",
    loadChildren:
      "./registersensor/registersensor.module#RegistersensorPageModule"
  },
  {
    path: "details/:id",
    loadChildren: "./details/details.module#DetailsPageModule"
  },
  {
    path: "details",
    loadChildren: "./details/details.module#DetailsPageModule"
  },
  {
    path: "detailnotification",
    loadChildren:
      "./detailnotification/detailnotification.module#DetailnotificationPageModule"
  },
  {
    path: "detailnotification/:id",
    loadChildren:
      "./detailnotification/detailnotification.module#DetailnotificationPageModule"
  }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
