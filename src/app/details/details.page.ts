import { MasterDataService } from './../services/master-data.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController, LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-details',
  templateUrl: './details.page.html',
  styleUrls: ['./details.page.scss'],
})
export class DetailsPage implements OnInit {

  todo: any = {
    name: '',
    serial_number: ''
  };

  todoId = null;

  constructor(private route: ActivatedRoute, private nav: NavController, private todoService: MasterDataService, private loadingController: LoadingController) { }

  ngOnInit() {
    this.todoId = this.route.snapshot.params['id'];
    if (this.todoId) {
      this.loadTodo();
    }
  }

  async loadTodo() {
    const loading = await this.loadingController.create({
      message: 'Cargando....'
    });
    await loading.present();

    this.todoService.getDevice(this.todoId).subscribe(todo => {
      loading.dismiss();;
      this.todo = todo;
    });
  }

  async saveTodo() {
    const loading = await this.loadingController.create({
      message: 'Guardando ....'
    });
    await loading.present();

    if (this.todoId) {
      this.todoService.updateDevice(this.todo, this.todoId).then(() => {
        loading.dismiss();
        this.nav.navigateForward('/home/tabs/tab3');
      });
    } else {
      this.todoService.addDevice(this.todo).then(() => {
        loading.dismiss();
        this.nav.navigateForward('/home/tabs/tab3');
      });
    }
  }
  async onRemoveTodo(idTodo: string) {
    this.todoService.removeDevice(idTodo);
  }
}