import { MasterDataService } from './../services/master-data.service';
import { Component } from '@angular/core';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {

  devices: any[];

  constructor(private dataservice: MasterDataService) { }

  ngOnInit() {
    this.dataservice.getDevices().subscribe((devices) => {
      console.log('Todoss', devices);
      this.devices = devices;
    })
  }
  onRemove(id: string) {
    this.dataservice.removeDevice(id);
  }
}
