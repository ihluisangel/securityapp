import { EventsService } from "./../services/events.service";
import { Component } from "@angular/core";

@Component({
  selector: "app-tab2",
  templateUrl: "tab2.page.html",
  styleUrls: ["tab2.page.scss"]
})
export class Tab2Page {
  events: any[];
  constructor(private eventservice: EventsService) {}

  ngOnInit() {
    this.eventservice.getEvents().subscribe(res => {
      console.log("Todoss", res);
      this.events = res;

      console.log(res);
    });
  }
  getDate(item): string {
    let date = new Date(Number(item.seconds * 1000)).toISOString();
    return date;
  }
}
