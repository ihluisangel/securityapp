import { Injectable } from "@angular/core";
import * as firebase from 'firebase/app';
import { AngularFirestore } from '@angular/fire/firestore';
@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  private snapshotChangesSubscription: any;

  constructor(private firestore: AngularFirestore) { }

  registerUserv1(value) {
    return new Promise<any>((resolve, reject) => {
      firebase.auth().createUserWithEmailAndPassword(value.email, value.password)
        .then(
          res => resolve(res),
          err => reject(err))
    })
  }

  registerUser(user) {
    return new Promise<any>((resolve, reject) => {
      firebase.auth().createUserWithEmailAndPassword(user.email, user.password).then((newUser) => {
        resolve(newUser);
        ;

        this.snapshotChangesSubscription = this.firestore.collection('users').add({
          username: user.email,
          fullname: user.fullname,
          address: user.address,
          id_auth: newUser.user.uid,
          status: true,
          list_devices: '',
          list_events: '',
          created: Date(),
        })
      })
    })
  }

  loginUser(value) {
    return new Promise<any>((resolve, reject) => {
      firebase.auth().signInWithEmailAndPassword(value.email, value.password)
        .then(
          res => resolve(res),
          err => reject(err))
    })
  }

  logoutUser() {
    return new Promise((resolve, reject) => {
      if (firebase.auth().currentUser) {
        firebase.auth().signOut()
          .then(() => {
            console.log("LOG Out");
            resolve();
          }).catch((error) => {
            reject();
          });
      }
    })
  }

  userDetails() {
    return firebase.auth().currentUser;
  }

  setUser(user) {
    return this.firestore.collection('users').add(user);
  }

  getUsers() {
    return this.firestore.collection('users').snapshotChanges();
  }
  getUserBy(uid) {
    return new Promise<any>((resolve, reject) => {
      this.snapshotChangesSubscription = this.firestore.collection('users', ref => ref.where('id_auth', '==', uid)).snapshotChanges()
        .subscribe(snapshots => {
          resolve(snapshots);
          this.snapshotChangesSubscription.unsubscribe();
        })
    });
  }
  updateUser(id, user) {
    this.firestore.doc('users/' + id).update(user);
  }

  deleteUser(id) {
    this.firestore.doc('users/' + id).delete();
  }
}