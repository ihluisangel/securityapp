import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';
@Injectable({
  providedIn: 'root'
})
export class MasterDataService {
  private todosCollection: AngularFirestoreCollection<any>;
  private todos: Observable<any[]>;

  constructor(db: AngularFirestore) {
    this.todosCollection = db.collection<any>('devices');
    this.todos = this.todosCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );
  }

  getDevices() {
    return this.todos;
  }
  getDevice(id: string) {
    return this.todosCollection.doc<any>(id).valueChanges();
  }
  updateDevice(todo: any, id: string) {
    return this.todosCollection.doc(id).update(todo);
  }
  addDevice(todo: any) {
    return this.todosCollection.add(todo);
  }
  removeDevice(id: string) {
    return this.todosCollection.doc(id).delete();
  }
}
