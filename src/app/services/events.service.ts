import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import {
  AngularFirestoreCollection,
  AngularFirestore
} from "@angular/fire/firestore";

@Injectable({
  providedIn: "root"
})
export class EventsService {
  private eventsCollection: AngularFirestoreCollection<any>;
  private eventsall: Observable<any[]>;

  constructor(db: AngularFirestore) {
    this.eventsCollection = db.collection<any>("events");
    this.eventsall = this.eventsCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );
  }

  getEvents() {
    return this.eventsall;
  }
  getEvent(id: string) {
    return this.eventsCollection.doc<any>(id).valueChanges();
  }
  updateEvent(todo: any, id: string) {
    return this.eventsCollection.doc(id).update(todo);
  }
  addEvent(todo: any) {
    return this.eventsCollection.add(todo);
  }
  removeEvent(id: string) {
    return this.eventsCollection.doc(id).delete();
  }
}
