import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistersensorPage } from './registersensor.page';

describe('RegistersensorPage', () => {
  let component: RegistersensorPage;
  let fixture: ComponentFixture<RegistersensorPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistersensorPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistersensorPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
