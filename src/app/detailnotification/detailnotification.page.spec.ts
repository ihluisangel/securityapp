import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailnotificationPage } from './detailnotification.page';

describe('DetailnotificationPage', () => {
  let component: DetailnotificationPage;
  let fixture: ComponentFixture<DetailnotificationPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailnotificationPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailnotificationPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
