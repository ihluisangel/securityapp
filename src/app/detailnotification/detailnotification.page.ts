import { ActivatedRoute } from "@angular/router";
import { EventsService } from "./../services/events.service";
import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl
} from "@angular/forms";
import { NavController } from "@ionic/angular";

@Component({
  selector: "app-detailnotification",
  templateUrl: "./detailnotification.page.html",
  styleUrls: ["./detailnotification.page.scss"]
})
export class DetailnotificationPage implements OnInit {
  public data: any = {
    date: {
      seconds: 1576290203
    }
  };
  id: any = null;

  constructor(
    public navCtrl: NavController,
    private eventService: EventsService,
    private router: ActivatedRoute,
    private cdr: ChangeDetectorRef
  ) {}
  ngOnInit() {
    this.id = this.router.snapshot.params["id"];
    this.eventService.getEvent(this.id).subscribe((res: any) => {
      console.log("este es mi response", res);
      this.data = res;
      this.cdr.detectChanges();
    });
  }
  getDate(item): string {
    let date = new Date(Number(item.seconds * 1000)).toISOString();
    return date;
  }
}
