// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBn2YF4tweyKI-PSEIAadKHSqwX8HvUu8I",
    authDomain: "ionic-fcm-ea491.firebaseapp.com",
    databaseURL: "https://ionic-fcm-ea491.firebaseio.com",
    projectId: "ionic-fcm-ea491",
    storageBucket: "ionic-fcm-ea491.appspot.com",
    messagingSenderId: "7144731815",
    appId: "1:7144731815:web:2516187d2fed1bbcf31782"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
